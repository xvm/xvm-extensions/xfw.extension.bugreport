# Changelog

## v.1.0.7

* update sentry.native to v0.4.6
* change server DSN

## v.1.0.6

* update sentry.native to v0.4.4
* change server DSN

## v.1.0.5

* report xfw.loader version as the release

## v.1.0.4

* update sentry-native

## v1.0.3

* fix version report

## v1.0.2

* fix crash on client exit

## v1.0.1

* fix user id hashing

## v1.0.0

* first version as separate wotmod package