"""
This file is part of the XFW BugReport project.

Copyright (c) 2018-2021 XFW BugReport Team.

XFW BugReport is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, version 3.

XFW BugReport is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

#
# Imports
#

#cpython
import logging
import os.path

#bigworld/wot
from gui.shared.utils import getPlayerDatabaseID
from helpers import dependency
from skeletons.gui.app_loader import IAppLoader, GuiGlobalSpaceID

#xfw.loader
import xfw_loader.python as loader

#xfw.bugreport
from native import XFWBugReportNative

#
# Constants
#

SENTRY_DSN_NATIVE = 'https://137ae9ed542741e1aba76cdbc66eeb49@sentry.openwg.net/2'
SENTRY_DSN_PYTHON = ''

XFW_BUGREPORTING_PRIVACY = 'https://privacy.openwg.net/'
XFW_BUGREPORTING_PACKAGENAME = 'com.modxvm.xfw.bugreport'

#
# Globals
#

g_xfw_bugreport_initialized = False
g_xfw_bugreport_native = None
g_xfw_bugreport_python = None


#
# handlers
#

def __on_gui_space_entered(spaceID):
    global g_xfw_bugreport_native
    global g_xfw_bugreport_python

    user_id = ""

    if spaceID == GuiGlobalSpaceID.LOGIN:
        user_id = "0"
    elif spaceID == GuiGlobalSpaceID.LOBBY:
        user_id = str(getPlayerDatabaseID())
    else:
        return

    if g_xfw_bugreport_native is not None:
        g_xfw_bugreport_native.user_set(user_id)

    if g_xfw_bugreport_python is not None:
        g_xfw_bugreport_python.user_set(user_id)


#
# xfw
#

def xfw_is_module_loaded():
    return g_xfw_bugreport_initialized


def xfw_module_init():

    global g_xfw_bugreport_initialized
    global g_xfw_bugreport_native

    logger = logging.getLogger('XFW/BugReport')

    #init
    g_xfw_bugreport_native = XFWBugReportNative()
    if not g_xfw_bugreport_native.options_init():
       logger.error("[xfw_module_init] failed to initialize native bug reporting")
       return

    #dsn
    g_xfw_bugreport_native.options_dsn(SENTRY_DSN_NATIVE)

    #consent
    consent_required = False
    if loader.get_client_realm() == 'RU':
        logger.info("[xfw_module_init] bugreporting does not require user consent because of CIS region. Please add empty 'XFW_BUGREPORT_OPTOUT.txt' file to game root to disable it")
        consent_required = False
    else:
        logger.info("[xfw_module_init] bugreporting requires user consent. Please add empty 'XFW_BUGREPORT_OPTIN.txt' file to game root to enable it")
        consent_required = True
    g_xfw_bugreport_native.options_consent_required(consent_required)

    #environment
    environment = 'public'
    if os.path.exists('wargaming_qa.conf'):
        environment = 'wargaming_qa'
    g_xfw_bugreport_native.options_environment(environment)

    #release
    g_xfw_bugreport_native.options_release(loader.WOT_VERSION_FULL.replace(' ', '_'))

    #initialize
    if not g_xfw_bugreport_native.start():
        return False

    #userset
    if not g_xfw_bugreport_native.user_set("0"):
        logger.error("[xfw_module_init] native bug reports failed to install. (failed to set userid)")
        return False
    dependency.instance(IAppLoader).onGUISpaceEntered += __on_gui_space_entered

    #tags
    g_xfw_bugreport_native.tag_set("version_wot", loader.WOT_VERSION_FULL)

    #mods_list = loader.get_mod_ids()
    #for mod_id, mod_version in mods_list.iteritems():
    #    g_xfw_bugreport_native.tag_set("version_%s" % mod_id, mod_version)

    #consent
    if os.path.exists('XFW_BUGREPORT_OPTOUT.txt'):
        g_xfw_bugreport_native.consent_set(False)
        logger.info("[xfw_module_init] bugreporting disabled because of user opt-out")
    elif os.path.exists('XFW_BUGREPORT_OPTIN.txt'):
        g_xfw_bugreport_native.consent_set(True)
        logger.info("[xfw_module_init] bugreporting enabled because of user opt-in")
    elif os.path.exists('wargaming_qa.conf'):
        g_xfw_bugreport_native.consent_set(True)
        logger.info("[xfw_module_init] bugreporting enabled because of QA")

    #finish message
    logger.info("[xfw_module_init] bugreporting was initialized successfuly. Please read our privacy policy:")
    logger.info("[xfw_module_init] %s" % XFW_BUGREPORTING_PRIVACY)
    g_xfw_bugreport_initialized = True

    return g_xfw_bugreport_initialized
