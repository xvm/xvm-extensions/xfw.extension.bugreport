"""
This file is part of the XFW BugReport project.

Copyright (c) 2018-2021 XFW BugReport Team.

XFW BugReport is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, version 3.

XFW BugReport is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

#cpython
import logging
import os.path

#xfw.loader
import xfw_loader.python as loader

class XFWBugReportNative():
    def __init__(self):
        self.__package_name = 'com.modxvm.xfw.bugreport'
        self.__logger = logging.getLogger('XFW/Crashreport/Native')

    def options_init(self):

        #load xfw native
        xfwnative = loader.get_mod_module('com.modxvm.xfw.native')
        if not xfwnative:
            self.__logger.error('[start] Failed to load XFW Native module.')
            return False

        #load xfw crashreport native module
        self.__native_module = xfwnative.unpack_load_native(self.__package_name, 'xfw_bugreport.pyd', 'XFW_BugReport')
        if not self.__native_module:
            self.__logger.error('[start] Failed to load XFW BugReport module.')
            return False

        #restore set unhandled exception handler function
        if not self.__native_module.uef_initialize():
            self.__logger.error("[start] Failed to initialize UEF manager.")
            return False

        #create crashreporter object
        self.__native_object = self.__native_module.BugReporter()

        #check compatibility
        if not self.__native_object.is_platform_supported():
            self.__logger.warning("[start] Native bug reports are not not supported on this platform.")
            return False

        #options/databasepath
        if not self.__native_object.options_databasepath_set(u'%s/%s/db' % (unicode(loader.XFWLOADER_TEMPDIR), unicode(self.__package_name))):
            self.__logger.error("[xfw_module_init] Native bug reports failed to install. (failed to set database path)")
            return False

        #options/attachments
        if os.path.exists('game.log'):
            self.__native_object.options_attachment_add(u"game.log")
        self.__native_object.options_attachment_add(u"python.log")
        self.__native_object.options_attachment_add(u"xvm.log")

        return True

    def options_consent_required(self, consent):
        return self.__native_object.options_consent_required_set(consent)

    def options_dsn(self, dsn):
        return self.__native_object.options_dsn_set(dsn)

    def options_environment(self, environment):
        return self.__native_object.options_environment_set(environment)

    def options_release(self, release):
        return self.__native_object.options_release_set(release)

    def start(self):
        if not self.__native_object.initialize():
            self.__logger.error("[xfw_module_init] Crash reports failed to install. (failed to initialize sentry)")
            return False

        self.__native_object.tag_append_exe_version('version_executable')
        return True

    def user_set(self, user_id):
        return self.__native_object.user_set(user_id)

    def tag_set(self, tag_key, tag_val):
        return self.__native_object.tag_set(tag_key, tag_val)

    def consent_set(self, consent):
        return self.__native_object.consent_set(consent)
