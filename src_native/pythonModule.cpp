﻿/**
 * This file is part of the XVM Framework project.
 *
 * Copyright (c) 2018-2021 XVM Team.
 *
 * XVM Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * XVM Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <pybind11/pybind11.h>

#include "crashreporter.h"
#include "uef_manager.h"

PYBIND11_MODULE(XFW_BugReport, m) {
    m.doc() = "XFW Crashreport module";

    m.def("uef_initialize", &UEFManager::Initialize, "Initialize UEF manager");

    pybind11::class_<CrashReporter>(m, "BugReporter")
        .def(pybind11::init<>())

        //initialization
        .def("is_platform_supported", &CrashReporter::is_platform_supported)
        .def("is_initialized", &CrashReporter::is_initialized)
        .def("initialize", &CrashReporter::initialize)
        .def("shutdown", &CrashReporter::shutdown)

        //options
        .def("options_attachment_add", &CrashReporter::options_attachment_add)
        .def("options_consent_required_set", &CrashReporter::options_consent_required_set)
        .def("options_databasepath_set", &CrashReporter::options_databasepath_set)
        .def("options_dsn_set", &CrashReporter::options_dsn_set)
        .def("options_environment_set", &CrashReporter::options_environment_set)
        .def("options_release_set", &CrashReporter::options_release_set)

        //tag
        .def("tag_append_exe_version", &CrashReporter::tag_append_exe_version)
        .def("tag_set", &CrashReporter::tag_set)

        //user
        .def("user_set", &CrashReporter::user_set)

        //consent
        .def("consent_get", &CrashReporter::consent_get)
        .def("consent_set", &CrashReporter::consent_set);
}
