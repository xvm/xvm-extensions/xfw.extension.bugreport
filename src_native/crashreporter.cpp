#include <algorithm>
#include <cstring>

#include "digestpp.hpp"

#include <Windows.h>

#include "common.h"
#include "dllMain.h"
#include "crashreporter.h"




CrashReporter::CrashReporter()
{
    if (GetModuleHandleW(library_name.c_str()) == nullptr) {
        library_handle = LoadLibraryW((GetModuleDirectory(hDLL) / library_name).c_str());
    }

    //create options
    _options = sentry_options_new();
    sentry_options_set_logger(_options, CrashReporter::logger, nullptr);
    sentry_options_set_debug(_options, 1);
}


CrashReporter::~CrashReporter()
{
    shutdown();

    FreeLibrary(library_handle);
}


bool CrashReporter::is_platform_supported()
{

    return true;
}


bool CrashReporter::is_initialized()
{
    return _initialized;
}


bool CrashReporter::options_release_set(const std::string& version) {
    if (is_initialized())
        return false;

    sentry_options_set_release(_options, version.c_str());
    return true;
}

bool CrashReporter::options_databasepath_set(const std::wstring& path)
{
    if (is_initialized())
        return false;


    auto path_n = (GetWorkingDirectory() / std::wstring(path)).native();
    std::replace(path_n.begin(), path_n.end(), L'/', L'\\');
    sentry_options_set_database_pathw(_options, path_n.c_str());
    return true;
}


bool CrashReporter::options_attachment_add(const std::wstring& filepath) {
    if (is_initialized())
        return false;

    sentry_options_add_attachmentw(_options, (GetWorkingDirectory() / filepath).c_str());
    return true;
}

bool CrashReporter::options_consent_required_set(bool val)
{
    if (is_initialized())
        return false;

    sentry_options_set_require_user_consent(_options, val);
    return true;
}


bool CrashReporter::options_dsn_set(const std::string& dsn) {
    if (is_initialized())
        return false;

    sentry_options_set_dsn(_options, dsn.c_str());
    return true;
}

bool CrashReporter::options_environment_set(const std::string& environment)
{
    if (is_initialized())
        return false;

    sentry_options_set_environment(_options, environment.c_str());
    return true;
}

bool CrashReporter::initialize()
{

    int result = sentry_init(_options);

    _initialized = result == 0;

    if (_initialized) {
        sentry_set_level(SENTRY_LEVEL_ERROR);
    }

    return _initialized;
}


bool CrashReporter::shutdown()
{
    sentry_shutdown();
    _initialized = false;
    return true;
}

bool CrashReporter::consent_get()
{
    switch (sentry_user_consent_get()) {
        case SENTRY_USER_CONSENT_GIVEN:
            return true;
        case SENTRY_USER_CONSENT_UNKNOWN:
        case SENTRY_USER_CONSENT_REVOKED:
        default:
            return false;
    }

    return false;
}


bool CrashReporter::consent_set(bool val)
{
    if (!is_initialized()) {
        return false;
    }

    if (val) {
        sentry_user_consent_give();
    }
    else {
        sentry_user_consent_revoke();
    }
    return true;
}


bool CrashReporter::tag_set(const std::string& key, const std::string& value) {
    if (!is_initialized()) {
        return false;
    }

    sentry_set_tag(key.c_str(), value.c_str());
    return true;
}

bool CrashReporter::tag_append_exe_version(const std::string& key)
{
    if (!is_initialized()) {
        return false;
    }

    auto ver = GetModuleVersion(static_cast<HMODULE>(nullptr));
    sentry_set_tag(key.c_str(), ver.c_str());
    return true;
}

std::string CrashReporter::get_machine_guid()
{
    std::string key = "SOFTWARE\\Microsoft\\Cryptography";
    std::string name = "MachineGuid";

    HKEY hKey = 0;
    DWORD type = 0;
    DWORD cbData = 0;

    if (RegOpenKeyExA(HKEY_LOCAL_MACHINE, key.c_str(), 0, KEY_READ | KEY_WOW64_64KEY, &hKey) != ERROR_SUCCESS) {
        return "";
    }

    if (RegQueryValueExA(hKey, name.c_str(), NULL, &type, NULL, &cbData) != ERROR_SUCCESS) {
        RegCloseKey(hKey);
        return "";
    }

    if (type != REG_SZ) {
        RegCloseKey(hKey);
        return "";
    }

    std::string value(cbData, '\0');
    if (RegQueryValueExA(hKey, name.c_str(), NULL, NULL, reinterpret_cast<LPBYTE>(&value[0]), &cbData) != ERROR_SUCCESS) {
        RegCloseKey(hKey);
        return "";
    }

    RegCloseKey(hKey);

    value.erase(value.find('\0'));
    return value;
}

void CrashReporter::logger(sentry_level_t level, const char* message, va_list args, void* userdata)
{
    char format[2048]{};
    strcpy(format, "[xfw/sentry] ");
    strcat(format, message);

    char buf[2048]{};

    vsprintf(buf, format, args);
    OutputDebugStringA(buf);
}

bool CrashReporter::user_set(const std::string& user_id) {

    //calculate hash
    auto machine_guid = get_machine_guid();
    if (machine_guid.size() == 0) {
        return false;
    }

    //user_hash = SHA256(SHA256(<machine_guid>)+SHA256(<user_id>))
    auto machine_guid_hash = digestpp::sha256().absorb(machine_guid).hexdigest();
    auto user_id_hash = digestpp::sha256().absorb(user_id.c_str(), strlen(user_id.c_str())).hexdigest();
    auto user_hash = digestpp::sha256().absorb(machine_guid_hash);

    sentry_value_t user = sentry_value_new_object();
    if (user_id.size() > 0) {
        sentry_value_set_by_key(user, "id", sentry_value_new_string(user_hash.hexdigest().c_str()));
        sentry_value_set_by_key(user, "username", sentry_value_new_string(user_id.c_str()));
    }

    sentry_set_user(user);
    return true;
}