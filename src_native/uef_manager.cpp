/**
 * This file is part of the XVM Framework project.
 *
 * Copyright (c) 2018-2021 XVM Team.
 *
 * XVM Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * XVM Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <Windows.h>
#include <Tlhelp32.h>
#include <process.h>

#include <MinHook.h>

#include "uef_manager.h"

SetUnhandledExceptionFilterTypedef UEFManager::SetUnhandledExceptionFilter_trampoline = nullptr;

LPTOP_LEVEL_EXCEPTION_FILTER UEFManager::handler_main = nullptr;
LPTOP_LEVEL_EXCEPTION_FILTER UEFManager::handler_other = nullptr;


bool UEFManager::Initialize()
{
    UEFManager::handler_main = SetUnhandledExceptionFilter(UEFManager::UnhandledExceptionFilter);

    hook();

    return true;
}

bool UEFManager::Deinitialize()
{
    unhook();

    SetUnhandledExceptionFilter(UEFManager::handler_main);
    return false;
}

bool UEFManager::hook()
{
    MH_STATUS mh_status = MH_Initialize();

    if (mh_status != MH_OK && mh_status != MH_ERROR_ALREADY_INITIALIZED) {
        return false;
    }

    if (MH_CreateHook(&SetUnhandledExceptionFilter, &UEFManager::SetUnhandledExceptionFilter_hook, (LPVOID*)(&UEFManager::SetUnhandledExceptionFilter_trampoline)) != MH_OK) {
        return false;
    }

    if (MH_EnableHook(&SetUnhandledExceptionFilter) != MH_OK) {
        return false;
    }

}

bool UEFManager::unhook()
{
    MH_DisableHook(&SetUnhandledExceptionFilter);
    return true;
}

LPTOP_LEVEL_EXCEPTION_FILTER __stdcall UEFManager::SetUnhandledExceptionFilter_hook(LPTOP_LEVEL_EXCEPTION_FILTER lpTopLevelExceptionFilter)
{
    auto& [startpos, endpos] = UEFManager::getExecutableMemoryRange();

    //get caller address
    typedef USHORT(WINAPI* CaptureStackBackTraceType)(ULONG, ULONG, PVOID*, PULONG);
    CaptureStackBackTraceType captureStackBackTraceType = (CaptureStackBackTraceType)(GetProcAddress(LoadLibraryW(L"kernel32.dll"), "RtlCaptureStackBackTrace"));
    if (captureStackBackTraceType == nullptr) {
        return nullptr;
    }
    const int kMaxCallers = 62; //requirement for XP
    void* callers[kMaxCallers]{};
    int callers_count = (captureStackBackTraceType)(0, kMaxCallers, callers, nullptr);
    if (callers_count < 2) {
        return nullptr;
    }

    //assign hook
    char* addr = reinterpret_cast<char*>(callers[1]);
    if (addr >= startpos && addr <= endpos) {
        UEFManager::handler_main = lpTopLevelExceptionFilter;
    }
    else {
        UEFManager::handler_other = lpTopLevelExceptionFilter;
    }

    return nullptr;
}

LONG __stdcall UEFManager::UnhandledExceptionFilter(_EXCEPTION_POINTERS* ExceptionInfo)
{
    LONG result = 0;

    if (UEFManager::handler_other != nullptr) {
        result = UEFManager::handler_other(ExceptionInfo);
    }

    if (UEFManager::handler_main != nullptr) {
        result = UEFManager::handler_main(ExceptionInfo);
    }

    return result;
}


std::pair<char*, char*> UEFManager::getExecutableMemoryRange()
{
    WCHAR lpFilename[2048]{};
    GetModuleFileNameW(NULL, lpFilename, 2048);
    char* startpos = reinterpret_cast<char*>(GetModuleHandleW(lpFilename));

    MODULEENTRY32W lpme{};
    BOOL Result = 0;
    DWORD modsize = 0;

    HANDLE hSnap = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, _getpid());
    if (hSnap)
    {
        lpme.dwSize = sizeof(MODULEENTRY32W);
        Result = Module32FirstW(hSnap, &lpme);

        while (Result)
        {
            if (wcsstr(lpFilename, lpme.szModule))
            {
                modsize = lpme.modBaseSize;
                break;
            }
            Result = Module32NextW(hSnap, &lpme);
        }
        CloseHandle(hSnap);
    }

    return std::make_pair(startpos, startpos + modsize);
}