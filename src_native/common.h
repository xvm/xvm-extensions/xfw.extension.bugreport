#pragma once

#include <string>
#include <filesystem>

#include <Windows.h>

template<typename ... Args>
std::string string_format_a(const std::string& format, Args ... args);

template<typename ... Args>
std::wstring string_format_w(const std::wstring& format, Args ... args);

std::wstring string_to_wstring(const std::string& str);

std::string GetModuleVersion(HMODULE hModule);

std::string GetModuleVersion(const wchar_t* moduleName);

std::filesystem::path GetModuleDirectory(HMODULE hModule);

std::filesystem::path GetWorkingDirectory();
