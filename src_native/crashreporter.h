#pragma once

#include <string>

#include "sentry.h"

class CrashReporter {

public:
    CrashReporter();
    ~CrashReporter();

    //general getters
    bool is_platform_supported();
    bool is_initialized();

    //options (should be executed before initialization)
    bool options_attachment_add(const std::wstring& filepath);
    bool options_consent_required_set(bool val);
    bool options_dsn_set(const std::string& dsn);
    bool options_environment_set(const std::string& environment);
    bool options_release_set(const std::string& version);
    bool options_databasepath_set(const std::wstring& path);

    //initialization
    bool initialize();
    bool shutdown();

    //consent
    bool consent_get();
    bool consent_set(bool val);

    //tag
    bool tag_set(const std::string& key, const std::string& value);
    bool tag_append_exe_version(const std::string& key);

    //user
    bool user_set(const std::string& user_id);

private:
    sentry_options_t* _options = nullptr;
    bool _initialized = false;

    std::wstring library_name = L"sentry.dll";
    HMODULE library_handle = nullptr;

    static std::string get_machine_guid();
    static void logger(sentry_level_t level, const char* message, va_list args, void* userdata);
};
