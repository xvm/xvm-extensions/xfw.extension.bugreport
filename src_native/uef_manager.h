/**
 * This file is part of the XVM Framework project.
 *
 * Copyright (c) 2018-2021 XVM Team.
 *
 * XVM Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * XVM Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <utility>

#include <Windows.h>


typedef LPTOP_LEVEL_EXCEPTION_FILTER(WINAPI* SetUnhandledExceptionFilterTypedef)(LPTOP_LEVEL_EXCEPTION_FILTER lpTopLevelExceptionFilter);


class UEFManager {
public:
    static bool Initialize();
    static bool Deinitialize();
private:

    static bool hook();
    static bool unhook();

    //SetUnhandledExceptionFilter hook
    static SetUnhandledExceptionFilterTypedef SetUnhandledExceptionFilter_trampoline;
    static LPTOP_LEVEL_EXCEPTION_FILTER WINAPI SetUnhandledExceptionFilter_hook(LPTOP_LEVEL_EXCEPTION_FILTER lpTopLevelExceptionFilter);

    //SetUnhandledExceptionFilter handler
    static LONG WINAPI UnhandledExceptionFilter(_EXCEPTION_POINTERS* ExceptionInfo);


    //addresses
    static LPTOP_LEVEL_EXCEPTION_FILTER handler_main;
    static LPTOP_LEVEL_EXCEPTION_FILTER handler_other;

    //helpers
    static std::pair<char*, char*> getExecutableMemoryRange();
};
